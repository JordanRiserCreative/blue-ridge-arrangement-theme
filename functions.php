<?php
	add_theme_support( 'post-thumbnails' );

	function blue_ridge_register_theme_styles() {
	    wp_register_style( 'theme-styles', get_template_directory_uri().'/css/site.css');
	    wp_enqueue_style( 'theme-styles' );
	}
	add_action( 'wp_enqueue_scripts', 'blue_ridge_register_theme_styles' );

	function blue_ridge_scripts() {
		wp_register_script('main', get_template_directory_uri().'/js/main.js', array('jquery'),null, true);
		wp_register_script('select', get_template_directory_uri().'/js/select.js', array('main'),null, true);

		wp_enqueue_script('select');
		wp_enqueue_script('main');
	}
	add_action( 'wp_enqueue_scripts', 'blue_ridge_scripts' );

	function blue_ridge_theme_customizer( $wp_customize ) {
    $wp_customize->add_section('blue_ridge_image_section' , array(
	    'title'       => __( 'Images', 'blue_ridge' ),
	    'priority'    => 30,
	    'description' => 'Upload a logo to replace the default site name and description in the header',
		));
		$wp_customize->add_setting('blue_ridge_logo');
		$wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'blue_ridge_logo', array(
	    'label'    => __( 'Home Page Large Logo', 'blue_ridge' ),
	    'section'  => 'blue_ridge_image_section',
	    'settings' => 'blue_ridge_logo',
		)));
		$wp_customize->add_setting('blue_ridge_header_logo');
		$wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'blue_ridge_header_logo', array(
	    'label'    => __( 'Header Logo', 'blue_ridge' ),
	    'section'  => 'blue_ridge_image_section',
	    'settings' => 'blue_ridge_header_logo',
		)));
		$wp_customize->add_setting('blue_ridge_background');
		$wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'blue_ridge_background', array(
	    'label'    => __( 'Background Image', 'blue_ridge' ),
	    'section'  => 'blue_ridge_image_section',
	    'settings' => 'blue_ridge_background',
		)));
	}
	add_action('customize_register', 'blue_ridge_theme_customizer');

	add_filter( 'wp_title', 'blue_ridge_homepage_wp_title' );
	function blue_ridge_homepage_wp_title( $title )
	{
	  if( empty( $title ) && ( is_home() || is_front_page() ) ) {
	    return ' | Home';
	  }
	  return $title;
	}
?>
