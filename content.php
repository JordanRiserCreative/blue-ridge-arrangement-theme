<article class="post">
	<?php
		if ( is_single() ) {
			echo '<div class="author-info">';
				the_title( '<h3 class="title">', '</h3>' );
				echo '<span class="meta">'; ?>
					<?php the_author(); ?> | <?php echo human_time_diff(get_the_time('U'), current_time('timestamp')); ?> ago
				<?php echo '</span>';
			echo '</div>';
			if(isset($blogimages[0]['blog-image-inside'])){
				$postImage = wp_get_attachment_image($blogimages[0]['blog-image-inside'],'full');
				echo $postImage;
			}else if(isset($blogimages[0]['blog-image-main'])){
				$postImage = wp_get_attachment_image($blogimages[0]['blog-image-main'],'full',false,array('class'=>'blog-image-main'));
				echo $postImage;
			}
		} else {
			the_title( '<h3 class="title"><a href="' . esc_url( get_permalink() ) . '">', '</a></h3>' );
			if(isset($blogimages[0]['blog-image-front'])){
				$postImage = wp_get_attachment_image($blogimages[0]['blog-image-front'],'full');
				echo $postImage;
			}else if(isset($blogimages[0]['blog-image-main'])){
				$postImage = wp_get_attachment_image($blogimages[0]['blog-image-main'],'full',false,array('class'=>'blog-image-main'));
				echo $postImage;
			}
		}
	?>
	<?php if(is_single()){ ?>
	<div class="post">
		<?php the_content(); ?>
		<?php comments_template(); ?>
	</div>
	<?php }else{ ?>
	<span class="meta">
		<?php the_author(); ?> | <?php echo human_time_diff(get_the_time('U'), current_time('timestamp')); ?> ago
	</span>
	<span class="tags">
		<img src="<?php echo bloginfo('template_directory'); ?>/images/tag-icon.png" class="tag-icon" width="25"><?php the_tags('',',',''); ?>
	</span>
	<div class="excerpt"><?php the_excerpt(); ?></div>
	<a class="btn read-more" href="<?php echo get_permalink(); ?>">Read More</a>
	<?php } ?>
</article>