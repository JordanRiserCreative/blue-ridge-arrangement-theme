(($) ->
	$('header').find('#toggle').click ->
		$(this).toggleClass 'on'
		$('nav#main_nav').toggleClass 'open'

	$(window).scroll ->
		if($(this).scrollTop() > 100)
			$('header').addClass 'scrolled'
		else
			$('header').removeClass 'scrolled'

	# Switch image src for paypal buttons
	$('.buttons').find('input[type=image]').attr 'src', templateUrl+'/images/purchase-button.png'
) jQuery