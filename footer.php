<footer>
	<div class="wrap logo">
		<img src="<?php echo bloginfo('template_directory'); ?>/images/blue-ridge-arrangement-footer-logo.png" class="footer-logo">
	</div>
	<div class="wrap links">
		<a href="<?php echo esc_url( home_url( '/' ) ); ?>">Home</a>
		<a href="<?php echo get_permalink( get_page_by_path( 'bio' ) ); ?>">Bio</a>
		<a href="<?php echo get_permalink( get_page_by_path( 'tour' ) ); ?>">Tour</a>
		<a href="<?php echo get_permalink( get_page_by_path( 'store' ) ); ?>">Store</a>
		<a href="#" class="eModal-1">Contact</a>
		<a href="<?php echo get_permalink( get_page_by_path( 'blog' ) ); ?>">Blog</a>
	</div>
	<div class="wrap social">
		<a href="https://www.twitter.com/bratheband" target="new"><img src="<?php echo bloginfo('template_directory'); ?>/images/icon-twitter-white.png"></a>
		<a href="https://www.facebook.com/blueridgearrangement/" target="new"><img src="<?php echo bloginfo('template_directory'); ?>/images/icon-facebook-white.png"></a>
		<a href="http://www.instagram.com/bratheband" target="new"><img src="<?php echo bloginfo('template_directory'); ?>/images/icon-instagram-white.png"></a>
		<a href="https://www.youtube.com/channel/UCn9AZt8CYAHUNskWFjhPVVg" target="new"><img src="<?php echo bloginfo('template_directory'); ?>/images/icon-youtube-white.png"></a>
	</div>
	<div class="wrap copy">
		<p>
			Copyright &copy; <?php the_time('Y'); ?> - Blue Ridge Arrangement. All Rights Reserved.
		</p>
	</div>
</footer>
<?php wp_footer(); ?>
</body>
</html>