<?php 
/*
	Template Name: Store
*/	
?>
<?php get_header(); ?>
	<section id="top">
		<div class="wrap">
			<div class="col">
				<h1>Store</h1>
			</div>
		</div>
	</section>
	<section id="content" class="store">
		<div class="wrap">
			<div class="col-10">
				<article class="post-single">
					<?php the_post_thumbnail(); ?>
					<aside class="product-single-content">
						<h3 class="title"><?php the_title(); ?></h3>
						<h4 class="price">$<?php echo get_cfc_field('price','price'); ?></h4>
						<div class="desc"><?php echo $post->post_content ?></div>
						<div class="buttons">
							<?php echo do_shortcode(get_cfc_field( 'purchase','paypal-button' )); ?>
						</div>
					</aside>
				</article>
			</div>
		</div>
	</section>
<?php get_footer(); ?>