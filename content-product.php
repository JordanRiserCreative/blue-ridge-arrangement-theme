<article class="product">
	<?php
		echo '<div class="product-image">';
		the_post_thumbnail();
		echo '</div>';
		echo '<h3 class="title">' . get_the_title() . '</h3>';
		echo '<h4 class="price">$' . get_cfc_field( 'price','price' ) . '</h4>';
		echo '<a class="button" href="'.get_the_permalink().'">More Info</a>';
	?>
</article>