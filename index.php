<?php get_header(); ?>
<section id="top">
	<div class="wrap">
		<div class="col">
			<h1>Blog</h1>
		</div>
	</div>
</section>
<section id="content">
	<aside class="wrap">
		<div class="blog-content">
			<div class="wrap">
				<div class="col-8">
					<?php
						if ( have_posts() ) {
							// Start the Loop.
							while ( have_posts() ) : the_post();

								/*
								 * Include the post format-specific template for the content. If you want to
								 * use this in a child theme, then include a file called called content-___.php
								 * (where ___ is the post format) and that will be used instead.
								 */
								get_template_part( 'content', get_post_format() );

							endwhile;
							if(function_exists('wp_pagenavi')) { 
								wp_pagenavi(); 
							}

						} else {
							// If no content, include the "No posts found" template.
							get_template_part( 'content', 'none' );
						}
					?>
				</div>
			</div>
		</div>
	</aside>
</section>
<?php get_footer(); ?>