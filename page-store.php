<?php 
/*
	Template Name: Store
*/	
?>
<?php get_header(); ?>
	<section id="top">
		<div class="wrap">
			<div class="col">
				<h1>Store</h1>
			</div>
		</div>
	</section>
	<section id="content" class="store">
		<div class="wrap">
			<div class="col-12">
				<div class="row">
						<?php 
							$args = array(
								'post_type' => 'product'
							);
							// The Query
							$the_query = new WP_Query( $args );

							// The Loop
							if ( $the_query->have_posts() ) {
								while ( $the_query->have_posts() ) {
									$the_query->the_post();
    							get_template_part( 'content', get_post_type() );
								}
							} else {
								// no posts found
							}
							/* Restore original Post Data */
							wp_reset_postdata();
						 ?>
				</div>
			</div>
		</div>
	</section>
<?php get_footer(); ?>