<?php get_header(); ?>
<?php while ( have_posts() ) : the_post(); ?>
	<section id="top" style="background-image: url(<?php echo (get_theme_mod('blue_ridge_background') ? esc_url(get_theme_mod('blue_ridge_background')) : bloginfo('template_directory')."/images/blue-ridge-arrangement-background-image.jpg") ?>)">
		<div class="wrap">
			<div class="col">
			<?php if(is_front_page()){ ?>
				<?php if ( get_theme_mod( 'blue_ridge_logo' ) ) : ?>
				  <img src='<?php echo esc_url( get_theme_mod( 'blue_ridge_logo' ) ); ?>' alt='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>'>
				<?php else : ?>
					<img src="<?php echo bloginfo('template_directory'); ?>/images/blue-ridge-arrangement-image-logo.png">
				<?php endif; ?>
			<?php } else {?>
				<h1><?php the_title(); ?></h1>
			<?php } ?>
			</div>
		</div>
	</section>
<?php endwhile; ?>
<?php wp_footer(); ?>
