module.exports = function(grunt) {
	
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),

		// Coffeeify for coffescript and browserify compatibility
		coffee: {
			dev: {
				files: {
					expand: true,
			    flatten: true,
			    cwd: 'coffee',
			    src: ['*.coffee'],
			    dest: 'js/',
			    ext: '.js'
				}
			},
		},

		// Concatenate javascripts from vendor directory
		concat: {
			options: {
				sourceMap: true,
			},
			dev: {
				src: ['coffee/vendor/**/*.js'],
				dest: 'js/vendor.js'
			}
		},

		// Compile Sass
		compass: {
			dev: {
			}
		},

		// Minify css
		cssmin: {
			build: {
				options: {
					keepSpecialComments: 0
				},
				files: {
					'css/style.css': ['css/_bower.css','css/style.css']
				}
			}
		},

		// Uglify javascript
		uglify: {
			build: {
				files: {
					'js/main.js': ['js/_bower.js','js/vendor.js','js/main.js']
				}
			}
		},

		clean: {
			dev: {
				src: ['css','js']
			}
		},

		// Concatenate Bower Components - Runs everytime grunt dev is run
		bower_concat: {
			dev: {
				dest: 'js/_bower.js',
				cssDest: 'css/_bower.css',
				exclude: ['jquery','html5shiv','jeet']
			}
		},

		// Watch for changes
		watch: {
			compass: {
				files: ['sass/**/*.{scss,sass}'],
				tasks: ['compass:dev']
			},
			markup: {
				files: ['**/*.{php, html}']
			},
			options: {
				livereload: true,
				spawn: false,
				interrupt: true
			}
		},

	});

	require('load-grunt-tasks')(grunt);

	grunt.registerTask('dev', ['bower_concat:dev','concat:dev','compass:dev','watch']);

	grunt.registerTask('clean-dev',['clean:dev']);

	grunt.registerTask('build', ['cssmin:build','uglify:build']);

}