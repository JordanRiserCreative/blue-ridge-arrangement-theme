<?php
/**
 * The Header for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) & !(IE 8)]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<title><?php bloginfo('name') ?><?php wp_title( '|', true, 'left' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
	<![endif]-->
	<script src="//use.typekit.net/pbo0nrj.js"></script>
	<script>try{Typekit.load();}catch(e){}</script>
	<script type="text/javascript">
		var templateUrl = '<?= get_bloginfo("template_url"); ?>';
	</script>
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-53785102-2', 'auto');
	  ga('send', 'pageview');

	</script>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

	<header class="site-header" role="banner">
		<div class="wrap main">
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
				<?php if ( get_theme_mod( 'blue_ridge_header_logo' ) ) : ?>
				  <img src='<?php echo esc_url( get_theme_mod( 'blue_ridge_header_logo' ) ); ?>' alt='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>' class="logo">
				<?php else : ?>
					<img src="<?php echo bloginfo('template_directory'); ?>/images/blue-ridge-arrangement-text-logo.png" alt="Blue Ridge Arrangement" class="logo">
				<?php endif; ?>
			</a>
			<div id="toggle">
				<div class="one"></div>
				<div class="two"></div>
				<div class="three"></div>
			</div>
		</div>
		<nav class="site-navigation primary-navigation" role="navigation" id="main_nav">
			<div class="scroll-wrap">
				<div class="row">
					<div class="col">
						<a href="<?php echo get_permalink( get_page_by_path( 'bio' ) ); ?>">
							<span class="text">Bio</span>
							<span class="img">
								<img class="nav-img" src="<?php echo bloginfo('template_directory'); ?>/images/nav-bg-1.png" alt="Bio">
							</span>
						</a>
					</div><!-- col -->
					<div class="col">
						<a href="<?php echo get_permalink( get_page_by_path( 'tour' ) ); ?>">
							<span class="text">Tour</span>
							<span class="img">
								<img class="nav-img" src="<?php echo bloginfo('template_directory'); ?>/images/nav-bg-2.png" alt="Tour">
							</span>
						</a>
					</div><!-- col -->
					<div class="col">
						<a href="<?php echo get_permalink( get_page_by_path( 'store' ) ); ?>">
							<span class="text">Store</span>
							<span class="img">
								<img class="nav-img" src="<?php echo bloginfo('template_directory'); ?>/images/nav-bg-3.png" alt="Store">
							</span>
						</a>
					</div><!-- col -->
				</div><!-- row -->
				<div class="row">
					<div class="col">
						<a href="#" class="eModal-1">
							<span class="text">Contact</span>
							<span class="img">
								<img class="nav-img" src="<?php echo bloginfo('template_directory'); ?>/images/nav-bg-4.png" alt="Contact">
							</span>
						</a>
					</div><!-- col -->
					<div class="col">
						<a href="<?php echo get_permalink( get_page_by_path( 'music' ) ); ?>">
							<span class="text">Music</span>
							<span class="img">
								<img class="nav-img" src="<?php echo bloginfo('template_directory'); ?>/images/nav-bg-5.png" alt="Music">
							</span>
						</a>
					</div><!-- col -->
					<div class="col social">
						<span class="img">
							<span class="social">
								<a href="https://www.twitter.com/bratheband"><img src="<?php echo bloginfo('template_directory'); ?>/images/icon-twitter-white.png"></a>
								<a href="https://www.facebook.com/blueridgearrangement/"><img src="<?php echo bloginfo('template_directory'); ?>/images/icon-facebook-white.png"></a>
								<a href="http://www.instagram.com/bratheband" target="new"><img src="<?php echo bloginfo('template_directory'); ?>/images/icon-instagram-white.png"></a>
								<a href="https://www.youtube.com/channel/UCn9AZt8CYAHUNskWFjhPVVg" target="new"><img src="<?php echo bloginfo('template_directory'); ?>/images/icon-youtube-white.png"></a>
							</span>
							<img class="nav-img" src="<?php echo bloginfo('template_directory'); ?>/images/nav-bg-6.png" alt="Social">
						</span>
					</div><!-- col -->
				</div><!-- row -->
			</div>
		</nav>
	</header><!-- #header -->
